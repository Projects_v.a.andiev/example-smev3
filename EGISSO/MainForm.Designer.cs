﻿namespace EGISSO
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.LocalMSZButton = new System.Windows.Forms.Button();
            this.ONMSZbutton = new System.Windows.Forms.Button();
            this.IsertProtocolbutton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SaveSignNumberButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.CreateSignCheckBox = new System.Windows.Forms.CheckBox();
            this.signNumberTextBox = new System.Windows.Forms.TextBox();
            this.Factbutton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SubscribeFileButton = new System.Windows.Forms.Button();
            this.PathForFileTextBox = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.lonelyChild = new System.Windows.Forms.Button();
            this.SignatureLonleyChildrenCb = new System.Windows.Forms.CheckBox();
            this.ExclusionLonelyChild = new System.Windows.Forms.Button();
            this.SignatureExclusionLonelyChild = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // LocalMSZButton
            // 
            this.LocalMSZButton.Location = new System.Drawing.Point(35, 55);
            this.LocalMSZButton.Name = "LocalMSZButton";
            this.LocalMSZButton.Size = new System.Drawing.Size(108, 28);
            this.LocalMSZButton.TabIndex = 0;
            this.LocalMSZButton.Text = "ЛМСЗ в xml";
            this.LocalMSZButton.UseVisualStyleBackColor = true;
            this.LocalMSZButton.Click += new System.EventHandler(this.LocalMSZButton_Click);
            // 
            // ONMSZbutton
            // 
            this.ONMSZbutton.Location = new System.Drawing.Point(35, 102);
            this.ONMSZbutton.Name = "ONMSZbutton";
            this.ONMSZbutton.Size = new System.Drawing.Size(108, 23);
            this.ONMSZbutton.TabIndex = 1;
            this.ONMSZbutton.Text = "ONMSZ в xml";
            this.ONMSZbutton.UseVisualStyleBackColor = true;
            this.ONMSZbutton.Click += new System.EventHandler(this.ONMSZbutton_Click);
            // 
            // IsertProtocolbutton
            // 
            this.IsertProtocolbutton.Location = new System.Drawing.Point(193, 102);
            this.IsertProtocolbutton.Name = "IsertProtocolbutton";
            this.IsertProtocolbutton.Size = new System.Drawing.Size(214, 23);
            this.IsertProtocolbutton.TabIndex = 3;
            this.IsertProtocolbutton.Text = "Вставить данные о протоколе в БД";
            this.IsertProtocolbutton.UseVisualStyleBackColor = true;
            this.IsertProtocolbutton.Click += new System.EventHandler(this.IsertProtocolbutton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SaveSignNumberButton);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.CreateSignCheckBox);
            this.groupBox1.Controls.Add(this.signNumberTextBox);
            this.groupBox1.Controls.Add(this.Factbutton);
            this.groupBox1.Location = new System.Drawing.Point(35, 147);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(603, 151);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Формирование и настройка для фактов назначения";
            // 
            // SaveSignNumberButton
            // 
            this.SaveSignNumberButton.Location = new System.Drawing.Point(280, 37);
            this.SaveSignNumberButton.Name = "SaveSignNumberButton";
            this.SaveSignNumberButton.Size = new System.Drawing.Size(239, 29);
            this.SaveSignNumberButton.TabIndex = 13;
            this.SaveSignNumberButton.Text = "Сохранить ЭЦП в конфиг";
            this.SaveSignNumberButton.UseVisualStyleBackColor = true;
            this.SaveSignNumberButton.Click += new System.EventHandler(this.SaveSignNumberButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(220, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Номер ЭЦП которой подписывают файлы";
            // 
            // CreateSignCheckBox
            // 
            this.CreateSignCheckBox.AutoSize = true;
            this.CreateSignCheckBox.Checked = true;
            this.CreateSignCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CreateSignCheckBox.Location = new System.Drawing.Point(280, 102);
            this.CreateSignCheckBox.Name = "CreateSignCheckBox";
            this.CreateSignCheckBox.Size = new System.Drawing.Size(239, 17);
            this.CreateSignCheckBox.TabIndex = 11;
            this.CreateSignCheckBox.Text = "Формировать отделяемую подписью p7s ";
            this.CreateSignCheckBox.UseVisualStyleBackColor = true;
            // 
            // signNumberTextBox
            // 
            this.signNumberTextBox.Location = new System.Drawing.Point(14, 42);
            this.signNumberTextBox.Name = "signNumberTextBox";
            this.signNumberTextBox.Size = new System.Drawing.Size(248, 20);
            this.signNumberTextBox.TabIndex = 10;
            // 
            // Factbutton
            // 
            this.Factbutton.Location = new System.Drawing.Point(14, 95);
            this.Factbutton.Name = "Factbutton";
            this.Factbutton.Size = new System.Drawing.Size(248, 28);
            this.Factbutton.TabIndex = 9;
            this.Factbutton.Text = "Формировать факты назначений в xml";
            this.Factbutton.UseVisualStyleBackColor = true;
            this.Factbutton.Click += new System.EventHandler(this.Factbutton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.SubscribeFileButton);
            this.groupBox2.Controls.Add(this.PathForFileTextBox);
            this.groupBox2.Location = new System.Drawing.Point(35, 317);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(604, 121);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Подписание файла по пути";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Выберите путь к файлу";
            // 
            // SubscribeFileButton
            // 
            this.SubscribeFileButton.Location = new System.Drawing.Point(6, 87);
            this.SubscribeFileButton.Name = "SubscribeFileButton";
            this.SubscribeFileButton.Size = new System.Drawing.Size(585, 28);
            this.SubscribeFileButton.TabIndex = 1;
            this.SubscribeFileButton.Text = "Подписать";
            this.SubscribeFileButton.UseVisualStyleBackColor = true;
            this.SubscribeFileButton.Click += new System.EventHandler(this.SubscribeFileButton_Click);
            // 
            // PathForFileTextBox
            // 
            this.PathForFileTextBox.Location = new System.Drawing.Point(6, 37);
            this.PathForFileTextBox.Name = "PathForFileTextBox";
            this.PathForFileTextBox.Size = new System.Drawing.Size(585, 20);
            this.PathForFileTextBox.TabIndex = 0;
            this.PathForFileTextBox.Click += new System.EventHandler(this.PathForFileTextBox_Click);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(666, 176);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(105, 66);
            this.button2.TabIndex = 11;
            this.button2.Text = "30271";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lonelyChild
            // 
            this.lonelyChild.Location = new System.Drawing.Point(44, 491);
            this.lonelyChild.Name = "lonelyChild";
            this.lonelyChild.Size = new System.Drawing.Size(299, 28);
            this.lonelyChild.TabIndex = 12;
            this.lonelyChild.Text = "Включение детей сирот";
            this.lonelyChild.UseVisualStyleBackColor = true;
            this.lonelyChild.Click += new System.EventHandler(this.lonelyChild_Click);
            // 
            // SignatureLonleyChildrenCb
            // 
            this.SignatureLonleyChildrenCb.AutoSize = true;
            this.SignatureLonleyChildrenCb.Checked = true;
            this.SignatureLonleyChildrenCb.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SignatureLonleyChildrenCb.Location = new System.Drawing.Point(387, 498);
            this.SignatureLonleyChildrenCb.Name = "SignatureLonleyChildrenCb";
            this.SignatureLonleyChildrenCb.Size = new System.Drawing.Size(239, 17);
            this.SignatureLonleyChildrenCb.TabIndex = 13;
            this.SignatureLonleyChildrenCb.Text = "Формировать отделяемую подписью p7s ";
            this.SignatureLonleyChildrenCb.UseVisualStyleBackColor = true;
            // 
            // ExclusionLonelyChild
            // 
            this.ExclusionLonelyChild.Location = new System.Drawing.Point(44, 534);
            this.ExclusionLonelyChild.Name = "ExclusionLonelyChild";
            this.ExclusionLonelyChild.Size = new System.Drawing.Size(299, 29);
            this.ExclusionLonelyChild.TabIndex = 14;
            this.ExclusionLonelyChild.Text = "Исключение детей сирот";
            this.ExclusionLonelyChild.UseVisualStyleBackColor = true;
            this.ExclusionLonelyChild.Click += new System.EventHandler(this.ExclusionLonelyChild_Click);
            // 
            // SignatureExclusionLonelyChild
            // 
            this.SignatureExclusionLonelyChild.AutoSize = true;
            this.SignatureExclusionLonelyChild.Checked = true;
            this.SignatureExclusionLonelyChild.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SignatureExclusionLonelyChild.Location = new System.Drawing.Point(387, 541);
            this.SignatureExclusionLonelyChild.Name = "SignatureExclusionLonelyChild";
            this.SignatureExclusionLonelyChild.Size = new System.Drawing.Size(239, 17);
            this.SignatureExclusionLonelyChild.TabIndex = 15;
            this.SignatureExclusionLonelyChild.Text = "Формировать отделяемую подписью p7s ";
            this.SignatureExclusionLonelyChild.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 594);
            this.Controls.Add(this.SignatureExclusionLonelyChild);
            this.Controls.Add(this.ExclusionLonelyChild);
            this.Controls.Add(this.SignatureLonleyChildrenCb);
            this.Controls.Add(this.lonelyChild);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.IsertProtocolbutton);
            this.Controls.Add(this.ONMSZbutton);
            this.Controls.Add(this.LocalMSZButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hello World";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button LocalMSZButton;
        private System.Windows.Forms.Button ONMSZbutton;
        private System.Windows.Forms.Button IsertProtocolbutton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox CreateSignCheckBox;
        private System.Windows.Forms.TextBox signNumberTextBox;
        private System.Windows.Forms.Button Factbutton;
        private System.Windows.Forms.Button SaveSignNumberButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button SubscribeFileButton;
        private System.Windows.Forms.TextBox PathForFileTextBox;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button lonelyChild;
        private System.Windows.Forms.CheckBox SignatureLonleyChildrenCb;
        private System.Windows.Forms.Button ExclusionLonelyChild;
        private System.Windows.Forms.CheckBox SignatureExclusionLonelyChild;
    }
}

