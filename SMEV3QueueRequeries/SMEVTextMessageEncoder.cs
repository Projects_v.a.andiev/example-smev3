﻿using log4net;
using SignatureHelper.SignatureCrypto;
using SMEV3QueueRequeries.DataBase;
using SMEV3QueueRequeries.DataBase.LoadingProtocol;
using SMEV3QueueRequeries.SMEV3;
using System;
using System.IO;
using System.ServiceModel.Channels;
using System.Text;
using System.Xml;
using System.Xml.Linq;

/// <summary>
/// Кастомный кодировщик. Обертка над стандартным WCF кодировщиком.
/// </summary>
namespace SMEV3QueueRequeries
{
    public class SMEVTextMessageEncoder : MessageEncoder
    {
        private SMEVTextMessageEncoderFactory factory;
        private string contentType;
        private MessageEncoder innerEncoder;

        private string _request;
        private ILog _log;
        private MessageLogging _messageLogging;

        public SMEVTextMessageEncoder()
        {
            log4net.Config.XmlConfigurator.Configure();
            _log = LogManager.GetLogger(typeof(SMEVTextMessageEncoder));
            _messageLogging = new MessageLogging(_log);
        }

        public SMEVTextMessageEncoder(SMEVTextMessageEncoderFactory factory) : this()
        {
            this.factory = factory;
            this.innerEncoder = factory.InnerMessageFactory.Encoder;
            this.contentType = this.factory.MediaType;
        }

        public override string ContentType
        {
            get
            {
                return this.contentType;
            }
        }

        public override string MediaType
        {
            get
            {
                return this.factory.MediaType;
            }
        }

        public override MessageVersion MessageVersion
        {
            get
            {
                return this.factory.MessageVersion;
            }
        }



        /// <summary/>
        /// <param name="requestResponse"/><param name="bufferManager"/><param name="contentType"/>
        /// <returns/>
        public override Message ReadMessage(ArraySegment<byte> requestResponse, BufferManager bufferManager, string contentType)
        {
            byte[] message = new byte[requestResponse.Count];
            Array.Copy((Array)requestResponse.Array, requestResponse.Offset, (Array)message, 0, message.Length);
            string response = new UTF8Encoding().GetString(message);

            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.LoadXml(response);

                XmlNodeList xl = xDoc.GetElementsByTagName("OriginalMessageId", "urn://x-artefacts-smev-gov-ru/services/message-exchange/types/1.1");

                if (xl.Count > 0)
                {
                    Directory.CreateDirectory(@"D:\temp\");
                    xDoc.Save(@"D:\temp\response_" + DateTime.Now.ToString("HHmmssfff") + ".xml");
                    var responseSmev = XDocument.Parse(response).ToString();
                    var parseProtocol = new ParseProtocol(_log);
                    var xmlOperation = new XMLOperation();                
                    var smevProtocol = xmlOperation.SMEVProtocolFromXML<Response>(xDoc.OuterXml);

                    var requestRejected = parseProtocol.GetRequestRejected(smevProtocol);                  

                    _messageLogging.SMEV3QueueRequestsResponse(smevProtocol.OriginalMessageId, _request, responseSmev, CheckResponse(xDoc), 
                        parseProtocol.TypeProtocol(xDoc.OuterXml),
                        smevProtocol.SenderProvidedResponseData.MessageID, requestRejected?.RejectionReasonCode.ToString() ?? null, 
                        requestRejected?.RejectionReasonDescription ?? null);
                }
                return this.innerEncoder.ReadMessage(requestResponse, bufferManager, contentType);
            }
            catch (Exception ex)
            {
                string Error = ex.GetBaseException().ToString();
                _log.Error($@"{Error}\n{response}");
                throw ex;
            }
        }

        /// <summary/>
        /// <param name="stream"/><param name="maxSizeOfHeaders"/><param name="contentType"/>
        /// <returns/>
        public override Message ReadMessage(Stream stream, int maxSizeOfHeaders, string contentType)
        {
            return this.innerEncoder.ReadMessage(stream, maxSizeOfHeaders, contentType);
        }

        /// <summary>
        /// Writes a message of less than a specified size to a byte array buffer at the specified offset
        /// 
        /// </summary>
        /// <param name="message"/><param name="maxMessageSize"/><param name="bufferManager"/><param name="messageOffset"/>
        /// <returns/>
        public override ArraySegment<byte> WriteMessage(Message message, int maxMessageSize, BufferManager bufferManager, int messageOffset)
        {
            byte[] messageRequest = this.innerEncoder.WriteMessage(message, maxMessageSize, bufferManager, messageOffset).Array;
            int length = messageRequest.Length;
            string requestMessage = Encoding.UTF8.GetString(messageRequest);

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(requestMessage);
            Directory.CreateDirectory(@"D:\temp\");
            xmlDocument.Save($@"D:\temp\request_{DateTime.Now.ToString("HHmmssfff")}.xml");

            //Сохранение в приватной переменной запрос обращения проверки очереди СМЭВ
            _request = xmlDocument.OuterXml;

            MemoryStream memoryStream = new MemoryStream();
            XmlWriter xmlWriter = XmlWriter.Create((Stream)memoryStream);

            xmlDocument.Save(xmlWriter);
            xmlWriter.Flush();
            byte[] buffer = memoryStream.GetBuffer();
            int num = (int)memoryStream.Position;
            memoryStream.Close();
            int bufferSize = num + messageOffset;
            byte[] array = bufferManager.TakeBuffer(bufferSize);
            Array.Copy((Array)buffer, 0, (Array)array, messageOffset, num);
            return new ArraySegment<byte>(array, messageOffset, num);
        }
        /// <summary>
        /// Writes a message to a specified stream
        /// 
        /// </summary>
        /// <param name="message"/><param name="stream"/>
        public override void WriteMessage(Message message, Stream stream)
        {
            this.innerEncoder.WriteMessage(message, stream);
        }

        private RespType CheckResponse(XmlDocument xDoc)
        {
            RespType result = RespType.DATA;

            if (xDoc.GetElementsByTagName("RequestRejected", "urn://x-artefacts-smev-gov-ru/services/message-exchange/types/1.1").Count > 0)
                return RespType.ERROR;

            return result;
        }
    }
}