﻿using Autofac;
using log4net;
using ServiceLauncher.Autofac.logger;
using ServiceLauncher.ChildrenHouseRight;
using ServiceLauncher.LetterForSmev.RetryRequest;
using System;
using System.Xml;

namespace ServiceLauncher
{
    class Program
    {
        static void Main(string[] args)
        {
            IContainer container = new ProgramLogger().BuildContainer();
            ILog log = container.Resolve<ILog>();
            var p = new Program();
            try
            {
                p.RunFact();
                p.RunChildNeedHouseRight();
                p.RunExclusionChildNeedHouseRigth();
                p.RetryErrorRequest(log);
            }
            catch (Exception ex)
            {
                log.Error(ex.GetBaseException().ToString());
            }
        }

        public void RunFact()
        {
            var services = new CreateAssigmentFact();

            services.CreateAssigmentFactXml(false);
        }

        public void RunChildNeedHouseRight()
        {
            var services = new CreatelonelyChildren();

            services.CreatelonelyChildrenXML(false);
        }

        public void RunExclusionChildNeedHouseRigth()
        {
            var services = new ExclusionLonelyChildren();

            services.CreateExclusionXML(false);
        }

        public void RetryErrorRequest(ILog log)
        {
            var retryRequest30271 = new ErorrsRequests(log);

            var errros = retryRequest30271.ErrorsInRequests();

            retryRequest30271.ReCallErrorsRequests(errros);
        }
    }
}
