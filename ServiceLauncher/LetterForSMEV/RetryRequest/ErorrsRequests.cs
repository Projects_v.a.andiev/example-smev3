﻿using Dapper;
using log4net;
using SignatureHelper.ClassesFromXSD.Assigment_Fact;
using SignatureHelper.DataBaseOperations;
using SignatureHelper.SignatureCrypto;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLauncher.LetterForSmev.RetryRequest
{
    public class ErorrsRequests
    {
        ILog _log;
        public ErorrsRequests(ILog log)
        {
            _log = log;
        }

        /// <summary>
        /// Повторный вызов запроса только для ошибок СМЭВ
        /// </summary>
        /// <returns></returns>
        public IEnumerable<dynamic> ErrorsInRequests()
        {
            try
            {
                string query =
                    @"
                    select 
	                    *
                    from 
	                        SMEV3QueueRequests s 
                    inner join 
	                    SMEV3LoggingRequest sl on s.RequestGUID = sl.EnvelopeGuid
                    where 
	                    s.ParseProtocolSuccess = 1  and  sl.StatusExecution = 0 and s.StateId = 1
	                    and TypeProtocol = 2
                        ";

                using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
                {
                    conn.Open();

                    return conn.Query(query);
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }


        public void ReCallErrorsRequests(IEnumerable<dynamic> requests)
        {
            try
            {
                foreach (var errorRequest in requests)
                {
                    var mailRequests = RequestsForRetry(errorRequest.RequestGUID.ToString());
                    foreach (var mail in mailRequests)
                    {
                        var createAssigmentFact = new CreateAssigmentFact();
                        var xmlOperation = new XMLOperation();
                        var fact = xmlOperation.DeSerializeXMLstring<data>(mail.CommonRequest);
                        createAssigmentFact.SendFact(fact);
                        SetRetryStatusExecution((int)errorRequest.id);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }




        private void SetRetryStatusExecution(int id)
        {
            try
            {

                string query = @"update [dbo].[SMEV3LoggingRequest] 
                set StatusExecution = @StatusExecution where id = @id ";

                using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
                {
                    conn.Open();

                    var protocol = new
                    {
                        StatusExecution = 4,
                        id
                    };

                    var result = conn.Execute(query, protocol);
                }

            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        public IEnumerable<dynamic> RequestsForRetry(string envelopeGuid)
        {
            try
            {
                string query =
                    @"Select 
                        * 
                    from 
                        [dbo].[SMEV3LoggingRequest]
                    where
                        EnvelopeGuid = @EnvelopeGuid and StatusExecution = 0
                        ";

                using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
                {
                    conn.Open();

                    var param = new
                    {
                        EnvelopeGuid = envelopeGuid
                    };

                    return conn.Query(query, param);
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }
    }


    //public enum RespType : byte
    //{
    //    DATA = 0,
    //    ERROR = 1,
    //    NO_DATA = 2,
    //    OTHER = 3, //custom user-defined error (LogsWS.RType 32bit)
    //    RETRY = 4 // Сервис поставщика недоступен, повторить инициализирующий запрос
    //}
}
