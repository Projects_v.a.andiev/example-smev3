﻿using Autofac;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLauncher.Autofac.logger
{
  public  class ProgramLogger
    {
        public  IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();
            log4net.Config.XmlConfigurator.Configure();
            builder.Register(c => LogManager.GetLogger(typeof(Object))).As<ILog>();
            return builder.Build();
        }
    }
}
