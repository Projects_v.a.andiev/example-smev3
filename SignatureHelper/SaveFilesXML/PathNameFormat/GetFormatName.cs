﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignatureHelper.SaveFilesXML.PathNameFormat
{
    public class FileNameXML
    {
        public string CodeOrganization { get; set; }
        public string CodeFile { get; set; }

        public string FileName
        {
            get { return $"{CodeOrganization}{CodeFile}"; }

        }

        //static GetFormatName(string codeOrganization, string codeFile)
        //{
        //    CodeOrganization = codeOrganization;
        //    CodeFile = codeFile;

        //}

        static FileNameXML()
        {

        }

    }
}
