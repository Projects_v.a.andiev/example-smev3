﻿using System;
using System.Configuration;
using System.Text.RegularExpressions;

namespace SignatureHelper.FolderFileOperation
{
    public static class PathSettings
    {

        public static string PathNamePlusDate(string path)
        {

            return $"{path}{DateTime.Today.ToString("dd-MM-yy")}\\"; ;
        }

        /// <returns></returns>
        public static string PathLocalPCFolder()
        {
            return ConfigurationManager.AppSettings["pathLocalPCFolder"];
        }

        public static string PathToMailservFACT()
        {
            return ConfigurationManager.AppSettings["pathToMailservFACT"];
        }

        public static string PathToMailservExclusionChildren()
        {
            return ConfigurationManager.AppSettings["pathToMailservExclusionChildren"];
        }

        public static string PathToMailservInclusionChildren()
        {
            return ConfigurationManager.AppSettings["pathToMailservInclusionChildren"];
        }

        public static string PathForSubscribe()
        {
            return ConfigurationManager.AppSettings["PathForSubscribe"];
        }

        public static string PathToMailservlMSZ()
        {
            return ConfigurationManager.AppSettings["pathToMailservlMSZ"];
        }

        public static string PathToMailservONMSZ()
        {
            return ConfigurationManager.AppSettings["pathToMailservONMSZ"];
        }

        public static string PathToMailservInsertReport()
        {
            return ConfigurationManager.AppSettings["pathToMailservInsertReport"];
        }

        public static string ServerName()
        {
            return ConfigurationManager.AppSettings["serverName"];
        }

        public static string BaseName()
        {
            return ConfigurationManager.AppSettings["baseName"];
        }

        public static string SerrialNumber()
        {
            var rgx = new Regex("[^a-fA-F0-9]");
            return rgx.Replace(ConfigurationManager.AppSettings["SignNumber"], string.Empty).ToUpper();
        }

        public static void AddOrUpdateAppSettings(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error writing app settings");
            }
        }
    }
}
