﻿namespace SignatureHelper.DataBaseOperations
{
    public class SqlQuery
    {
        //Запрос для получение локальных мер
        public const string QuerylocalMSZ = @"
           select 
/***new***/
distinct package.packageID, eggisoProtocol.recordID ID,eggWorkLMSZ.baseID,
/***new***/
                   convert ( varchar(20), localMSZ.[code_localMSZ]) code, [Title_localMSZ] title,
                   work.dateEnact, work.dateExpiration, work.periodicityCode ,
                   work.codePartKMSZ, classificationKMSZ.number_KMSZ codeMSZ, work.codeProvisionForm,
                   work.codeLevelNPA
                   ,
                   loc.id_localCategory IDCategory, loc.code_localCategory code, loc.title_localCategory title,
                   categorKMSZ.codeCategoryKMSZ,
                   Funding.codeFundingSource, funding.quota,
                   work.estimation, work.codeOKTMO
                   ,NPA.number_NPA number, NPA.Date_NPA [date], NPA.Title_NPA titleNPA, NPA.Authority_NPA authority
                   ,NPA.URL_NPA URL, work.KBKCode       
                ,work.id_work,
                                  /***new***/  
                                  provider.short_kod,
                                  information.name_extension
                                  /***new***/
                           ,estimation, eggWorkLMSZ.lastChanging, eggWorkLMSZ.typeWork_LMSZ                                                       
            from 
                   spr_EGISSO_localMSZ localMSZ
            inner join 
                   spr_EGISSO_work work on work.code_localMSZ = localMSZ.code_localMSZ
            inner join 
                   spr_EGISSO_kmsz_lmsz klmsz on  klmsz.code_localMSZ = localMSZ.code_localMSZ
            inner join 
                   spr_EGISSO_KMSZ classificationKMSZ on   classificationKMSZ.id_KMSZ = klmsz.id_KMSZ
            inner join 
                   spr_EGISSO_work_localCategory work_loc on work_loc.id_work = work.id_work 
            inner join 
                   spr_EGISSO_localCategory loc on loc.id =  work_loc.id_local
            inner join 
                   spr_EGISSO_CategoryKMSZ  categorKMSZ on categorKMSZ.id_Category = loc.id_Category 
            inner join 
                   spr_EGISSO_work_FundingSource work_Funding on work_Funding.id_work = work.id_work
            inner join
                   spr_EGISSO_FundingSource Funding on       Funding.id = work_Funding.id_FundingSource  
            inner join
                   spr_EGISSO_work_NPA work_NPA  on work_NPA.id_work = work.id_work and work_NPA.archive_tag = 0
            inner join
                   spr_EGISSO_NPA NPA on NPA.id_NPA = work_npa.id_NPA 
                              /***new***/
                 inner join 
                   egisso_Work_LMSZ eggWorkLMSZ on localMSZ.code_localMSZ  = eggWorkLMSZ.code_localMSZ  and eggWorkLMSZ.archive_tag = 0
                    inner join
                   egisso_protocolFA_Mena eggisoProtocol on eggisoProtocol.recordID = eggWorkLMSZ.id and eggisoProtocol.archive_tag = 0
            inner join
                   EGISSO_package package on package.packageID = eggisoProtocol.packageID and package.archive_tag = 0

                    inner join
                           spr_EGISSO_information_provider provider on provider.id_information_provider = package.information_provider and provider.archive_tag = 0
                    inner join
                           spr_EGISSO_type_information information on information.id_type_information = package.type_information and information.archive_tag = 0

/***new**/
            where 
                    localMSZ.archive_tag = 0
                    and
                              work.archive_tag  = 0 and 
                              /***new**/
                    package.packageState = 0 
                    and package.id_EGISSO_package =
                    (select max(package2.id_EGISSO_package) from EGISSO_package package2 where package.packageID = package2.packageID and package2.archive_tag = 0 )

				
                    ";

        //Запрос для получение ОНМСЗ
        public const string QuerylocalONMSZ = @"
        select 
	            contractID, egOrgDoc.title, number, contractor, dateEnact, dateExpiration
                ,egOrganization.id_organization, egOrganization.title titleOrganization, siteURL
	            , license, [address], email, egOrganization.INN legalINN, egOrganization.KPP legalKPP, INN
                ,localMSZ.ID_localMSZ localMSZID, egMFC.title titleMFC, town, street, house
	            , housing, building,  egMFC.note, egMFC.id_MFC
            from 
	            dbo.spr_EGISSO_organizationDoc egOrgDoc
            inner join 	
	            dbo.spr_EGISSO_Doc_organization egDocOrg on egOrgDoc.id_Doc = egDocOrg.id_Doc 
            inner join
	            dbo.spr_EGISSO_organization egOrganization on egOrganization.id_organization = egDocOrg.id_organization 
            inner join 
	            dbo.spr_EGISSO_organization_lmsz egOrgzLmsz on egOrgzLmsz.id_organization = egOrganization.id_organization
            left join
	            dbo.spr_EGISSO_organization_MFC egOrgMFC on egOrgMFC.id_organization = egOrganization.id_organization
            left join 
	            dbo.spr_EGISSO_MFC egMFC on egMFC.id_MFC = egOrgMFC.id_MFC
            inner join 
	            dbo.spr_EGISSO_localMSZ localMSZ on localMSZ.code_localMSZ = egOrgzLmsz.code_localMSZ

            where 
				egOrganization.archive_tag = 0
            ";


        //Запрос для получения ФАКТОВ НАЗНАЧЕНИЯ МСЗ (ASSIGNMENT-FACT)
        public const string QueryAssigmentFact = @"
        
select distinct --joined.id_group,
                                  package.packageID, eggisoProtocol.recordID, eggWorkFA.baseID, eggWorkFA.typeWork, paramFA.dateFinish,
                  eggWorkFA.ID ID, organization.OSZCode 
                   , personeMena.FamilyName,  personeMena.FirstName,personeMena.Patronymic
                   ,workLmsz.ID  LMSZID, localCateg.id_localCategory categoryID, paramFA.decision_date
                   ,paramFA.dateStart, paramFA.usingSign, paramFA.amount, paramFA.content, SNILS 
                   ,paramFA.comment 
                   ,mainEgisso.person_id, 
                              --linqMena.id_FamilyUD
                              case when joined.id_group is not null then (select id_familyud from EGISSO_main_joined_famUD  where id_group=joined.id_group and mainud=1  and archive_tag=0) else linqMena.id_FamilyUD end id_FamilyUD
                   ,Gender, BirthDate, Citizenship, docPersMena.VidDoc
                   ,Series, Number,isnull( IssueDate,'')IssueDate, Issuer
                   ,isnull( addresPersMena.Region,'')Region ,isnull(  addresPersMena.City,'') City, isnull( addresPersMena.District,'') District
                   ,isnull( addresPersMena.OKSMCode, '') OKSMCode, isnull(addresPersMena.PostIndex,'') PostIndex, isnull(addresPersMena.Apartment,'')Apartment
                   ,isnull(addresPersMena.Street,'')Street , isnull( addresPersMena.House,'') House, isnull( addresPersMena.Housing,'') Housing, 
                    paramFA.measuryCode, eggWorkFA.typeWork, eggWorkFA.lastChanging
                    ,provider.short_kod,information.name_extension                 
                    ,amount_Monetary, id_assignmentInfo
                    ,OKEICode, 
                                  --famUdMena.KodRn districtID,
                    case when joined.id_group is not null then (select f.kodrn from EGISSO_main_joined_famUD j,egisso_FamilyUD_mena f where 
                                  j.id_familyud=f.id_familyud and 
                                  j.id_group=joined.id_group and j.mainud=1  and j.archive_tag=0 and f.archive_tag=0) else famUdMena.KodRn end districtID
                                  
                                  
                                  ,isnull( mainEgisso.sam,0) sam , localMSZ.one_fact_family,
                     work_view.lmszProviderCode   
            from 
                   egisso_MainParametersFactAssignment_mena mainEgisso 
            inner join 
                   egisso_ParametersFA_mena paramFA on paramFA.id_MainParametersFA  = mainEgisso.id_MainParametersFA and paramFA.archive_tag = 0
            inner join 
                   egisso_SNILS_och_mena snilsOchMena on snilsOchMena.person_id = mainEgisso.person_id and snilsOchMena.archive_tag = 0
            inner join 
                   egisso_FIO_Persone_mena personeMena on personeMena.person_id = snilsOchMena.person_id and personeMena.archive_tag = 0
            left join 
                   egisso_Doc_Persone_mena docPersMena on docPersMena.person_id = snilsOchMena.person_id and docPersMena.archive_tag = 0
           left join 
                   egisso_Address_Persone_mena addresPersMena on addresPersMena.person_id = snilsOchMena.person_id and  addresPersMena.archive_tag = 0
            inner join 
                   egisso_linkFamilyUDFA_mena linqMena on linqMena.id_MainParametersFA = mainEgisso.id_MainParametersFA and linqMena.archive_tag = 0
            inner join 
                   egisso_FamilyUD_mena famUdMena on  linqMena.id_FamilyUD = famUdMena.id_FamilyUD and famUdMena.archive_tag = 0
            inner join
                   spr_EGISSO_work egissoWork on egissoWork.id_work = paramFA.id_work and egissoWork.archive_tag = 0
            inner join
                   spr_EGISSO_localMSZ localMSZ on localMSZ.code_localMSZ = egissoWork.code_localMSZ and localMSZ.archive_tag = 0
            inner join 
                   spr_EGISSO_organization_lmsz orgLmsz on orgLmsz.code_localMSZ = localMSZ.code_localMSZ and orgLmsz.archive_tag = 0
            inner join
                   spr_EGISSO_organization organization on organization.id_organization = orgLmsz.id_organization and organization.archive_tag = 0 and paramFA.OSZCode = organization.OSZCode
            inner join
                   spr_EGISSO_work_localCategory workLocalCateg on egissoWork.id_work = workLocalCateg.id_work and workLocalCateg.archive_tag = 0
            inner join
                   spr_EGISSO_localCategory localCateg on localCateg.id = paramFA.id_localcat and localCateg.archive_tag = 0
            inner join 
                    egisso_WorkFA_mena eggWorkFA on mainEgisso.id_MainParametersFA = eggWorkFA.id_MainParametersFA  and eggWorkFA.archive_tag = 0
            inner join
                    egisso_protocolFA_Mena eggisoProtocol on eggisoProtocol.recordID = eggWorkFA.id and eggisoProtocol.archive_tag = 0
            inner join
                    EGISSO_package package on package.packageID = eggisoProtocol.packageID and package.archive_tag = 0
            left join 
                    egisso_TerminationFA_mena termenation on mainEgisso.id_MainParametersFA = termenation.id_MainParametersFA  and termenation.archive_tag = 0
            inner join
                    spr_EGISSO_information_provider provider on provider.id_information_provider = package.information_provider and provider.archive_tag = 0
            inner join
                    spr_EGISSO_type_information information on information.id_type_information = package.type_information and information.archive_tag = 0
            inner join 
                    egisso_Work_LMSZ workLmsz on workLmsz.code_localMSZ = localMSZ.code_localMSZ and workLmsz.archive_tag = 0
            left join 
                    spr_EGISSO_measuryCode spr_measuryCode on spr_measuryCode.measuryCode=paramFA.measuryCode and spr_measuryCode.archive_tag = 0
                                              inner join 
                    spr_EGISSO_work_view work_view on work_view.code_localMSZ=localMSZ.code_localMSZ and work_view.archive_tag = 0 
left join EGISSO_main_joined_famUD joined on linqMena.id_familyud=joined.id_familyUD  and joined.archive_tag=0
left join EGISSO_main_joined_famUD joined2 on joined2.id_group=joined.id_group  and joined2.archive_tag=0


    where  
            mainEgisso.archive_tag = 0 and
            package.packageState = 0
            ---потом package.packageState  поставить в 0 
            and package.id_EGISSO_package =
            (select max(package2.id_EGISSO_package) from EGISSO_package package2 where package.packageID = package2.packageID and package2.archive_tag = 0 )


            ";


        //Запрос для получения данных по детям сиротам
        public const string QuerylonelyChildren = @"

select distinct					paramFA.create_date lastChanging, [provider].short_kod,
								information.name_extension,							
								  famUdMena.KodRn districtID,
                                  package.packageID ,
                                  eggisoProtocol.recordID uuid, 
                                  kmsz.number_kmsz codeMSZ,
                                  categorykmsz.codeCategoryKMSZ categoryMSZ,
                                  snilsOchMena.SNILS,
                                  paramFA.OSZCode, 
                    personeMena.FamilyName lastName,  
                                  personeMena.FirstName firstName,
                                  personeMena.Patronymic middleName,
                                  personeMena.BirthDate,


                                  paramFA.dateStart registryDate,
                                  paramFA.osnovanie reasonCode, 
                                  paramFA.OSZCode   codeOrg,
                               paramFA.decision_date reasonDate,

                   
                                  eggWorkFA.lastChanging        
            from 
                   egisso_MainParametersFactAssignment_mena mainEgisso 
            inner join 
                   egisso_ParametersFA_mena paramFA on paramFA.id_MainParametersFA  = mainEgisso.id_MainParametersFA and paramFA.archive_tag = 0
            inner join 
                   egisso_SNILS_och_mena snilsOchMena on snilsOchMena.person_id = mainEgisso.person_id and snilsOchMena.archive_tag = 0
            inner join 
                   egisso_FIO_Persone_mena personeMena on personeMena.person_id = snilsOchMena.person_id and personeMena.archive_tag = 0
              inner join
                   spr_EGISSO_work egissoWork on egissoWork.id_work = paramFA.id_work and egissoWork.archive_tag = 0
            inner join
                   spr_EGISSO_localMSZ localMSZ on localMSZ.code_localMSZ = egissoWork.code_localMSZ and localMSZ.archive_tag = 0
                 inner join
                   spr_EGISSO_kmsz_lMSZ kmszlMSZ on localMSZ.code_localMSZ = kmszlMSZ.code_localMSZ and kmszlMSZ.archive_tag = 0
                 inner join
                   spr_EGISSO_kmsz kmsz on kmszlMSZ.id_kmsz = kmsz.id_kMSZ and kmsz.archive_tag = 0
            inner join
                   spr_EGISSO_work_localCategory workLocalCateg on egissoWork.id_work = workLocalCateg.id_work and workLocalCateg.archive_tag = 0
            inner join
                   spr_EGISSO_localCategory localCateg on localCateg.id = paramFA.id_localcat and localCateg.archive_tag = 0
                    inner join
                              spr_egisso_categorykmsz categorykmsz on categorykmsz.id_Category =localCateg.id_category and categorykmsz.archive_tag=0

                    inner join 
                           egisso_WorkFA_mena eggWorkFA on mainEgisso.id_MainParametersFA = eggWorkFA.id_MainParametersFA  and eggWorkFA.archive_tag = 0
                    inner join
                           egisso_protocolFA_Mena eggisoProtocol on eggisoProtocol.recordID = eggWorkFA.id and eggisoProtocol.archive_tag = 0
                    inner join
                           EGISSO_package package on package.packageID = eggisoProtocol.packageID and package.archive_tag = 0
						   inner join 
                   egisso_linkFamilyUDFA_mena linqMena on linqMena.id_MainParametersFA = mainEgisso.id_MainParametersFA and linqMena.archive_tag = 0
            inner join 
                   egisso_FamilyUD_mena famUdMena on  linqMena.id_FamilyUD = famUdMena.id_FamilyUD and famUdMena.archive_tag = 0
			inner join
                    spr_EGISSO_information_provider [provider] on provider.id_information_provider = package.information_provider and provider.archive_tag = 0
			inner join
                    spr_EGISSO_type_information information on information.id_type_information = package.type_information and information.archive_tag = 0

            where  
                    mainEgisso.archive_tag = 0 and
                    package.packageState = 0 
                    and package.id_EGISSO_package =
                    (select max(package2.id_EGISSO_package) from EGISSO_package package2 where package.packageID = package2.packageID and package2.archive_tag = 0)


";

        //Исключение детей сирот
        public const string QueryExclusionLonelyChildren =
            @"

            select distinct                                
                                                       
                                                        [provider].short_kod,
                                  information.name_extension,                                               

                                  package.packageID ,
                                  eggisoProtocol.recordID uuid,
                                                        terminatFA.datefinish exclusionDate, 
                                                        terminatFA.reasonCodeExclusion reasonCode, 
                                                        paramFA.OSZCode   codeOrg,
                                                        eggWorkFA.baseID assignmentRightUuid,
                                                        eggWorkFA.lastChanging lastChanging     
            from 
                   egisso_MainParametersFactAssignment_mena mainEgisso 
            inner join 
                   egisso_ParametersFA_mena paramFA on paramFA.id_MainParametersFA  = mainEgisso.id_MainParametersFA and paramFA.archive_tag = 0
                 inner join 
                   egisso_terminationFA_mena terminatFA on terminatFA.id_MainParametersFA  = mainEgisso.id_MainParametersFA and terminatFA.archive_tag = 0

                    inner join 
                           egisso_WorkFA_mena eggWorkFA on mainEgisso.id_MainParametersFA = eggWorkFA.id_MainParametersFA  and eggWorkFA.archive_tag = 0
                    inner join
                           egisso_protocolFA_Mena eggisoProtocol on eggisoProtocol.recordID = eggWorkFA.id and eggisoProtocol.archive_tag = 0
                    inner join
                           EGISSO_package package on package.packageID = eggisoProtocol.packageID and package.archive_tag = 0
                    inner join
                    spr_EGISSO_information_provider [provider] on provider.id_information_provider = package.information_provider and provider.archive_tag = 0
                    inner join
                    spr_EGISSO_type_information information on information.id_type_information = package.type_information and information.archive_tag = 0

            where  
                    mainEgisso.archive_tag = 0 and
                    package.packageState = 0 
                    and package.id_EGISSO_package =
                    (select max(package2.id_EGISSO_package) from EGISSO_package package2 where package.packageID = package2.packageID and package2.archive_tag = 0)
                                  and typework=2--terminate


";
    }
}