﻿using Dapper;
using SignatureHelper.ClassesFromXSD.Assigment_Fact;
using SignatureHelper.ClassesFromXSD.lonelyChildren;
using SignatureHelper.ClassesFromXSD.lonelyChildren.xsd;
using SignatureHelper.SignatureCrypto;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace SignatureHelper.DataBaseOperations
{
    public class DataBaseOperation
    {
        /// <summary>
        ////Сохранение Сериализованного файла в БД
        /// </summary>
        /// <param name="xml"></param>
        public void InsertToBase(string xmlProtocol, string packageID)
        {
                string insertQuery = @"INSERT INTO [dbo].[EGISSO_PackageXML](packageID, XMLforSend) 
                VALUES (@packageID, @xml)";

                using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
                {
                    conn.Open();
                    
                    var result = conn.Execute(insertQuery, new
                    {
                        packageID,
                        xml = xmlProtocol
                    });
                }
            
        }

        public DataTable GetDataTableFromDB(string query)
        {
            var parameters = new List<SqlParameter>();

            SqlParameter param = new SqlParameter();

            //задаем значение параметра
            //param.ParameterName = "@District";
            //param.Value = districtID;
            //parameters.Add(param);

            var dt = GetDataTable.GetQuery(GetDataTable.ConnectionString(), query);

            return dt;
        }

        public void InsertProtocolOnDB(data dataPackage, DataBaseOperation dtOperation)
        {
            var xmlOperation = new XMLOperation();          
            var xmlReport = xmlOperation.GetXMLProtocol(dataPackage, dataPackage.package.packageId);
            lock (dtOperation)
            {
                dtOperation.InsertToBase(xmlReport, dataPackage.package.packageId);
            }
            
        }

        public void InsertProtocolOnDB(LonelyChildren.data dataPackage, DataBaseOperation dtOperation)
        {
            var xmlOperation = new XMLOperation();         
            var xmlReport = xmlOperation.GetXMLProtocol(dataPackage, dataPackage.package.packageId);
            lock (dtOperation)
            {
                dtOperation.InsertToBase(xmlReport, dataPackage.package.packageId);
            }
        }
    }
}
