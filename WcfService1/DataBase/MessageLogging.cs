﻿using SignatureHelper.DataBaseOperations;
using log4net;
using Smev3Services;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using SignatureHelper.SignatureCrypto;
using System.Diagnostics;
using System.Xml.Linq;
using System.Data.SqlTypes;
using System.Xml;
using SignatureHelper.ClassesFromXSD.Assigment_Fact;

namespace SMEV3Services.DataBase
{
    public class MessageLogging
    {
        private ILog _log;
        public MessageLogging(ILog log)
        {
            _log = log;
        }
        public void SaveLogRequest(CommonRequest cr)
        {
            try
            {
                string query =
                    @"INSERT INTO [dbo].[SMEV3LogRequest]
                        ([LogID], [StateID], [RequestID], [XmlRequest])
                        VALUES (@LogID, @StateID, @RequestID, @XmlRequest)";

                using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
                {
                    conn.Open();
                    var xmlConvert = new XMLOperation();

                    var result = conn.Execute(query, new
                    {                     
                        StateID = cr.StateID,
                        RequestID = cr.RequestID,
                        XmlRequest = XDocument.Parse(cr.MailEgisso)
                    });
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        public void LoggingSMEV3Request(CommonRequest cr, string Request, string Response, bool Error)
        {
            SMEV3LoggingRequest(cr, Request, Response, (Error) ? RespType.ERROR : RespType.DATA, 2,null,null);
        }

        public void SMEV3LoggingRequest(CommonRequest cr, string request, string response, RespType resp, int stackNum, 
            string packageId, XDocument mail)
        {
            try
            {
                string query =
                    @"INSERT INTO [dbo].[SMEV3LoggingRequest]
                        ([CommonRequest], [Request], [Response], [MethodName], 
                        [Error], [PackageId], [EnvelopeGuid] )
                        VALUES (@CommonRequest, @Request, @Response, @MethodName, @Error, @PackageId, @EnvelopeGuid)";

                using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
                {
                    conn.Open();

                    var result = conn.Execute(query, new
                    {
                        CommonRequest = mail,
                        Request = request,
                        Response = response,
                        MethodName = GetMethodName(stackNum),
                        Error = (byte)resp,
                        PackageId = packageId,
                        EnvelopeGuid = packageId
                    });
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        public string GetMethodName(int stackNum)
        {
            return
                (new StackTrace().GetFrames()[stackNum].GetMethod().Name == "_InvokeMethodFast") ?
                new StackTrace().GetFrames()[stackNum - 1].GetMethod().Name :
                new StackTrace().GetFrames()[stackNum].GetMethod().Name;
        }

        public string GetEnvelopeMessageId(string xml)
        {
            var guid = "";
            var doc = new XmlDocument();
            doc.LoadXml(xml);
            XmlNodeList elemList = doc.GetElementsByTagName("MessageId", @"urn://x-artefacts-smev-gov-ru/services/message-exchange/types/1.1");
            for (int i = 0; i < elemList.Count; i++)
            {
                if (elemList[i].Name.Equals("ns4:MessageId"))
                {
                    guid = elemList[i].InnerText;
                    i = elemList.Count;
                }
            }
            return guid;
        }


    }
    public enum RType : byte
    {
        REQ = 0,
        CHK = 1,
        CNL = 2,
        SYN = 3,
        ERR_REQ = 4,
        ERR_CHK = 5,
        ERR_CNL = 6,
        ERR_SYN = 7
    }
    public enum RespType : byte
    {
        DATA = 0,
        ERROR = 1,
        NO_DATA = 2,
        OTHER = 3 //custom user-defined error (LogsWS.RType 32bit)
    }
}