﻿using log4net;
using System;
using Autofac;
using Autofac.Integration.Wcf;
using System.ServiceModel;
using Smev3Services;
using SMEV3Services.RSID30271;

namespace SMEV3Services
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            var builder = new ContainerBuilder();
            // Register your service implementations.
            builder.RegisterType<Smev3Services.ServicesSMEV3>();         
            log4net.Config.XmlConfigurator.Configure();
            builder.Register(c => LogManager.GetLogger(typeof(object))).As<ILog>();

            builder.RegisterType<Service30271>();
            var container = builder.Build();
            AutofacHostFactory.Container = container;
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}