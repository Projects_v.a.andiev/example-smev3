﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Smev3Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    [ServiceKnownType(typeof(SMEV3Services.MessageMetadata)),
    ServiceKnownType(typeof(Response)),
    ServiceKnownType(typeof(SMEV3Services.InteractionStatusType))]
    public interface IService
    {
        [OperationContract]
        string GetData(int value);

        [OperationContract]
        CommonResponse rsid30042req(ref CommonRequest req);

        // TODO: Add your service operations here
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "Smev3Services.ContractType".
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }

    public class CommonRequest
    {
        public CommonRequest() { }
        /// <summary>
        /// Конструктор для нового объекта с копированием служебных полей
        /// </summary>
        /// <param name="origin"></param>
        public CommonRequest(CommonRequest origin)
        {
            this.IP = origin.IP;
            this.TabNumber = origin.TabNumber;
            this.ApplicationID = origin.ApplicationID;
            this.CallID = origin.CallID;
            this.PersonID = origin.PersonID;

            this.IsDouble = origin.IsDouble;

            this.LogID = origin.LogID;
            this.StateID = origin.StateID;
        }

        /// <summary>
        /// Идентификатор заявки (для асинхронных запросов)
        /// </summary>
        public string RequestID;

        public string FirstName;
        public string LastName;
        public string MiddleName;
        public string BirthDate;
        public string Gender;
        public string FullName;

        public string FirstName2;
        public string LastName2;
        public string MiddleName2;
        public string BirthDate2;
        public string Gender2;
        public string FullName2;

        public string FirstName3;
        public string LastName3;
        public string MiddleName3;

        public string DocType;
        public string DocSeries;
        public string DocNumber;
        public string DocDate;
        public string DocIssuer;

        public string DocType2;
        public string DocSeries2;
        public string DocNumber2;
        public string DocDate2;
        public string DocIssuer2;

        public string DataPeriod;
        public string StartPeriod;
        public string EndPeriod;

        public string MOCode;

        public string Snils;
        public string Snils2;

        // Индивидуальный номер налогоплательщика
        public string INN;

        public string Region;
        // Название населенного пункта или район
        public string Locality;
        public string Town;
        public string StreetCode;
        public string StreetName;
        public string HouseCode;
        public string HouseName;
        /// <summary>
        /// Код корпуса
        /// </summary>
        public string BuildingCode;
        /// <summary>
        /// Корпус
        /// </summary>
        public string Building;
        /// <summary>
        /// Код строения
        /// </summary>
        public string StructureCode;
        /// <summary>
        /// Строение
        /// </summary>
        public string Structure;
        public string FlatCode;
        public string Flat;
        /// <summary>
        /// Любая дополнительная информация вне установленных полей, 
        /// пока не используется, но все равно пусть будет.
        /// </summary>
        public string Aux;

        #region Служебные поля, не должны писаться в XML

        public string IP;
        /// <summary>
        /// Табельный номер инспектора
        /// </summary>
        public string TabNumber;
        /// <summary>
        /// Идентификатор приложения, использующего СМЭВ
        /// </summary>
        public string ApplicationID;
        /// <summary>
        /// Идентификатор обращения (номер информационного листа)
        /// </summary>
        public string CallID;
        /// <summary>
        /// Идентификатор персоны, участвующей в обращении
        /// </summary>
        public string PersonID;

        /// <summary>
        /// Признак наличия запроса - дубля
        /// </summary>
        public string IsDouble;

        /// <summary>
        /// Идентификатор записи в логе запросов к СМЭВ
        /// </summary>
        public int LogID;
        /// <summary>
        /// Успешность выполнения запроса 
        /// 1 - ошибка, 0 - окончание обработки заявки, 2 - заявка в состоянии обработки
        /// </summary>
        public int StateID;
        /// <summary>
        /// Уникальный идентификатор, связанный с запросом (для РосРеестра)
        /// </summary>
        public string GUID;

        #endregion
    }
    public class CommonResponse
    {
        private object response;
        private DateTime dateRequest;
        private DateTime dateResponse;
        private string comment, stateRequest;

        private string dateList;
        private string tabNumber;
        private string listNumber;

        public CommonResponse() { }
        public CommonResponse(object response, DateTime dateRequest, DateTime dateResponse)
        {
            this.response = response;
            this.dateRequest = dateRequest;
            this.dateResponse = dateResponse;
        }

        /// <summary>
        /// Обработанный ответ на запрос
        /// </summary>
        public object Response
        {
            get
            {
                return response;
            }
            set
            {
                response = value;
            }
        }
        /// <summary>
        /// Дата и время отправки запроса
        /// </summary>
        public DateTime DateRequest
        {
            get
            {
                return dateRequest;
            }
            set
            {
                dateRequest = value;
            }
        }
        /// <summary>
        /// Дата и время ответа на запрос
        /// </summary>
        public DateTime DateResponse
        {
            get
            {
                return dateResponse;
            }
            set
            {
                dateResponse = value;
            }
        }

        /// <summary>
        /// Дата обращения из информационного листа
        /// </summary>
        public string DateList
        {
            get
            {
                return dateList;
            }
            set
            {
                dateList = value;
            }
        }
        /// <summary>
        /// Табельный номер инспектора, отправившего запрос
        /// </summary>
        public string TabNumber
        {
            get
            {
                return tabNumber;
            }
            set
            {
                tabNumber = value;
            }
        }
        /// <summary>
        /// Номер информационного листа
        /// </summary>
        public string ListNumber
        {
            get
            {
                return listNumber;
            }
            set
            {
                listNumber = value;
            }
        }

        /// <summary>
        /// Список номеров найденных запросов - дублей
        /// </summary>
        public List<int> DoubleRequests;

        /// <summary>
        /// Примечания к запросу/ответу
        /// </summary>
        public string Comment
        {
            get
            {
                return comment;
            }
            set
            {
                comment = value;
            }
        }

        /// <summary>
        /// Текущий статус заявки, к которой относится запрс
        /// </summary>
        public string StateRequest
        {
            get
            {
                return stateRequest;
            }
            set
            {
                stateRequest = value;
            }
        }
    }
    public class SMEVRequest
    {
        private string request = String.Empty;

        public SMEVRequest() { }

        /// <summary>
        /// Оригинальный текст SOAP запроса к СМЭВ
        /// </summary>
        public string Request
        {
            get
            {
                return request;
            }
            set
            {
                request = value;
            }
        }
    }

    public class Property
    {
        public Property() { }

        public string Name;
        public string Value;
    }

    public class Response
    {
        public Response() { }

        public object MethodResponse;

        public string Type;
        public List<Property> Properties = new List<Property>();
        public byte[] File;
        public string Error;

        public bool AddProperty(string propName, string propValue)
        {
            if (!String.IsNullOrEmpty(propName) && !String.IsNullOrEmpty(propValue) &&
                !String.IsNullOrEmpty(propValue.Trim(new char[] { '-', ' ' })))
            {
                Properties.Add(new Property()
                {
                    Name = propName,
                    Value = propValue
                });
                return true;
            }
            return false;
        }
    }
}
